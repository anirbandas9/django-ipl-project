# Django IPL DATA PROJECT App

## Structure of IPL DATA PROJECT App

- Ipl Data Project Root
    ![ipl_project root folder](./screenshots/ipl_project.png)
- Ipl Data Project App
    ![ipl_project app](./screenshots/ipl_app.png)


## Installation
- First clone the repository using https/ssh:
  ```sh
  git clone {git:ssh/https}
  ```
- Install Virtual Environment:
  ```sh
  sudo apt-get install python3.10-venv
  ```
- Creating virtual env:
  ```sh
  python3 -m venv venv
  ```
- Activating virtual env:
  ```sh
  source venv/bin/activate
  ```
- Installing all the requirements for django poll app:
  ```sh
  pip install -r requirements.txt
  ```

<br>

## Running the App

- Make sure you are in the root directory where manage.py is present.
- Connected with **Postgresql** Database, so creation of same database with grant all permission with dbname, user, password all provided in the **src/my-site/settings.py** under DATABASE.
- To show migrations
  ```sh
  python manage.py makemigrations
  ```
- To complete migration
  ```sh
  python manage.py migrate
  ```
- To import data from csv to Postgres database
  ```sh
  python manage.py import_data
  ```
- Running the IPL DATA PROJECT App in port 8080
  ```sh
  python manage.py runserver 8080
  ```
- Go to the localhost:8080 from the browser to view the app
- Deactivate server with CONTROL+C
- Deactivate virtual env with:
  ```sh
  deactivate
  ```

## Login into Admin Panel

- Create superuser:
  ```sh
  python manage.py createsuperuser
  ```
- Enter username, email, password
- Go to localhost:8080/admin
- Enter username, password
- View the admin panel


## Screenshots

- Home Screen
![Home Screen](./screenshots/home.png)

### Problems
- Number of matches played per year for all the years in IPL.
![matches played per year](./screenshots/problem1.png)

- Number of matches won per team per year in IPL.
![matches won per team per year](./screenshots/problem2.png)

- Extra runs conceded per team in the year 2016
![extra runs conceded 2016](./screenshots/problem3.png)

- Top 10 economical bowlers in the year 2015
![top 10 economical bowlers 2015](./screenshots/problem4.png)

- Number of matches played API endpoint
- Can be accessed from the VIEW API button in respective chart
    ![matches played API](./screenshots/problem1_api.png)

- Top 10 economical bowlers API endpoint
    ![top 10 economical bowlers API](./screenshots/problem4_api.png)