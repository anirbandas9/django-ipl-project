from django.contrib import admin

from .models import Match, Delivery

admin.site.site_header = "IPL Admin"
admin.site.site_title = "IPL Admin Portal"
admin.site.index_title = "Welcome to IPL Researcher Portal"


# Filter match by season, city, winner
class filterMatch(admin.ModelAdmin):
    list_display = ('season', 'city', 'team1', 'team2', 'winner')
    # list_display = ('id', 'season', 'city', 'date', 'team1', 'team2', 'toss_winner', 'toss_decision', 'result',
    #                 'dl_applied', 'winner', 'wins_by_runs', 'wins_by_wickets', 'player_of_match', 'venue',
    #                 'umpire1', 'umpire2', 'umpire3')
    list_filter = ('season', 'city', 'winner')
    search_fields = ('season', 'city', 'winner')


# Filter delivery by batsman, bowler, batsman_runs
class filterDelivery(admin.ModelAdmin):
    list_display = ('match_id', 'inning', 'batting_team', 'bowling_team', 'over', 'ball', 'batsman', 'non_striker',
                    'bowler', 'is_super_over', 'wide_runs', 'bye_runs', 'legbye_runs', 'noball_runs', 'penalty_runs',
                    'batsman_runs', 'extra_runs', 'total_runs')
    list_filter = ('batting_team', 'bowling_team', 'ball', 'batsman_runs', 'extra_runs', 'total_runs')
    search_fields = ('batsman', 'bowler', 'batsman_runs')


admin.site.register(Match, filterMatch)
admin.site.register(Delivery, filterDelivery)
