// Fetch data from the API
fetch('/api/extra_runs_conceded_2016/')
.then(response => response.json())
.then(data => {
    // Render the HighCharts chart for extra runs conceded by each team in 2016
    Highcharts.chart('extra-runs-conceded-2016', {
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                borderRadius: 5,
                borderWidth: 0,
                pointWidth: 15,
                colorByPoint: true,
                states: {
                    hover: {
                        brightness: 0.2,
                        halo: {
                            size: 36
                        }
                    }
                }
            }
        },
        title: {
            text: 'Extra Runs Conceded By Each Team In 2016'
        },
        xAxis: {
            categories: data.map(team => team.bowling_team),
            title: {
                text: 'Team'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra Runs Conceded'
            }
        },
        series: [
            {
                name: 'Extra Runs Conceded',
                data: data.map(team => team.extra_runs_conceded),
                color: 'grey',
            }
        ],  
    });
});