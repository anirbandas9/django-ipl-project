// Fetch data from the API
fetch('/api/wins_per_team_per_year/')
.then(response => response.json())
.then(data => {
    const categories = Object.keys(data);
    // get unique teams
    const teams = [...new Set(Object.values(data).flatMap(Object.keys))];
    // create series for each team
    const series = teams.map(team => {
        return {
            name: team,
            data: categories.map(year => data[year][team] || 0)
        };
    });
    
    // Render the HighCharts chart for wins per team per year where data is a nested objects
    Highcharts.chart('wins-per-team-per-year', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Wins Per Team Per Year'
        },
        xAxis: {
            categories: categories,
            title: {
                text: 'Year'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Wins'
            }
        },
        series: series
    });
});