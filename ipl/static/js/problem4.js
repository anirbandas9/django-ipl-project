// Fetch data from the API
fetch('/api/top_economical_bowlers_2015/')
.then(response => response.json())
.then(data => {
    // Render the HighCharts chart for top economical bowlers in 2015
    Highcharts.chart('top-economical-bowlers-2015', {
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                borderRadius: 5,
                borderWidth: 0,
                pointWidth: 15,
                colorByPoint: true,
                states: {
                    hover: {
                        brightness: 0.2,
                        halo: {
                            size: 36
                        }
                    }
                }
            }
        },
        title: {
            text: 'Top Economical Bowlers In 2015'
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Bowler'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy'
            }
        },
        series: [
            {
                name: 'Economy',
                data: Object.values(data),
                color: 'grey',
            }]
    });
});