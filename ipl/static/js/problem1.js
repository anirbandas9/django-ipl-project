// Fetch data from the API
fetch('/api/matches_per_year/')
.then(response => response.json())
.then(data => {
    // Render the HighCharts chart
    Highcharts.chart('matches-per-year', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches Per Year'
        },
        xAxis: {
            categories: data.map(item => item.season),
            title: {
                text: 'Year'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches'
            }
        },
        series: [{
            name: 'Matches',
            data: data.map(item => item.matches),
            color: 'grey'
        }],
        plotOptions: {
            series: {
                borderRadius: 5,
                borderWidth: 0,
                pointWidth: 15,
                colorByPoint: true,
                states: {
                    hover: {
                        brightness: 0.2,
                        halo: {
                            size: 36
                        }
                    }
                }
            }
        },
    });
});